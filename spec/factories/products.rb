# encoding: utf-8
FactoryGirl.define do
  factory :product, :class => Product do
    title         "first product"
    description "wjp is gay"
    price         5.5
    image_url    "123456789.jpg"
  end

  factory :title_nil_prod do
    title
    description "wjp is a geeker"
    price         5.0
    image_url    "123.jpg"
  end

  factory :title_uniq_prod do
    title         "first product"
    description "wjp is a superman"
    price         4.38
    image_url    "456.jpg"
  end
end