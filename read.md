* rspec 测试主框架
* factory_girl  测试数据
* guard (guard-rspec)监测修改的文件 刷新自动进行测试 其实不仅用在测试中
* capybara 测试页面 模拟页面操作
* database_cleaner 测试结束后 清空test数据库
* spork 加速测试环境目前不支持rails4

spork-rails-3.2.1 only support Rails 3.0.0 - 3.2.x

----------------------------------------------------------------------------------------
###[Rspec](https://github.com/rspec/rspec-rails)
```rails generate rspec:install```

###[factory_girl](https://github.com/thoughtbot/factory_girl_rails)
```ruby
=>application.rb
  config.generators do |g|
    g.test_framework :rspec
    g.fixture_replacement :factory_girl, :dir => "spec/factories"
  end
```

### [guard](https://github.com/guard/guard-rspec)
```
guard init rspec
guard#进入命令行
```

### [Capybara](https://github.com/jnicklas/capybara#using-capybara-with-rspec)
